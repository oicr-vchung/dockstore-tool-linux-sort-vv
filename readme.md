[![Build Status](https://travis-ci.org/CancerCollaboratory/dockstore-tool-linux-sort.svg)](https://travis-ci.org/CancerCollaboratory/dockstore-tool-linux-sort)
[![Docker Repository on Quay](https://quay.io/repository/collaboratory/dockstore-tool-linux-sort/status "Docker Repository on Quay")](https://quay.io/repository/collaboratory/dockstore-tool-linux-sort)
